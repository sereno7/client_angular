// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,

  DAYACTIVITY:"https://localhost:44344/api/dayactivities/",
  URL_API_DAY: "https://localhost:44344/api/day/",
  URL_API_TEAM : "https://localhost:44344/api/teams/",
  URL_API_ACTIVITY : "https://localhost:44344/api/activity/",
  URL_API_LEADERBOARD : "https://localhost:44344/api/leaderboard/",
  URL_API_SECURITY : "https://localhost:44344/api/security/",
  URL_SIGNALR_GAME : "https://localhost:44344/gamehub/",

  // DAYACTIVITY:"https://localhost/api/dayactivities/",
  // URL_API_DAY: "https://localhost/api/day/",
  // URL_API_TEAM : "https://localhost/api/teams/",
  // URL_API_ACTIVITY : "https://localhost/api/activity/",
  // URL_API_LEADERBOARD : "https://localhost/api/leaderboard/",
  // URL_API_SECURITY : "https://localhost/api/security/",
  // URL_SIGNALR_GAME : "https://localhost/gamehub/",

  // DAYACTIVITY:"http://localhost:62866/api/dayactivities/",
  // URL_API_DAY: "http://localhost:62866/api/day/",
  // URL_API_TEAM : "http://localhost:62866/api/teams/",
  // URL_API_ACTIVITY : "http://localhost:62866/api/activity/",
  // URL_API_LEADERBOARD : "http://localhost:62866/api/leaderboard/",
  // URL_API_SECURITY : "http://localhost:62866/api/security/",
  // URL_SIGNALR_GAME : "http://localhost:62866/gamehub/",
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
