export const environment = {
  production: true,
  DAYACTIVITY:"https://localhost/api/dayactivities/",
  URL_API_DAY: "https://localhost/api/day/",
  URL_API_TEAM : "https://localhost/api/teams/",
  URL_API_ACTIVITY : "https://localhost/api/activity/",
  URL_API_LEADERBOARD : "https://localhost/api/leaderboard/",
  URL_API_SECURITY : "https://localhost/api/security/",
  URL_SIGNALR_GAME : "https://localhost/gamehub/",
};
