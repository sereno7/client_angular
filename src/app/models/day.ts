import { Team } from './team';

export interface Day{
    id:number;
    customerName: string;
    date:Date;
    newTeam?: Team;
    listTeams? : Team[];
    countActivity?:number;
    status?:number;
}
