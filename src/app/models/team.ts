import { Person } from './Person';

export interface Team{
    id :number;
    name?:string;
    color?:string;
    slogan?:string;
    totalPoint?: number;
    idDay?: number;
    newPlayer?: Person;
    listPlayers?: Person[];
    currentActivity?: number;
    oldActivity?: number;
}
