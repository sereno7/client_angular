export interface PlayerMasterPage{
        nameT:string, 
        idT:number,
        nameA: string,
        isReady: boolean,
        result?:number;
        color?:string;
    }
