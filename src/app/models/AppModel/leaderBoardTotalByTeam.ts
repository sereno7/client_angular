export interface LeaderBoardTotalByTeam{
    teamName:string,
    idTeam:number,
    idDay:number,
    total:number,
    color:string,
}
