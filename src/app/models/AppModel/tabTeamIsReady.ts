export interface tabTeamReadyModel{
    id:number;
    name:string,
    color?:string,
    nameCurrentActivity: string, 
    isReady: boolean,
}