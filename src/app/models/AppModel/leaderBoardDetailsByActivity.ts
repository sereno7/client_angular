export interface LeaderBoardDetailsByActivity{
    teamName:string,
    activityName:string,
    idActivity:number,
    idTeam:number,
    idDay:number,
    result:number,
    color:string;
}
