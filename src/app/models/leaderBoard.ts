export interface LeaderBoard{
    id?:number,
    result:number,
    idActivity:number,
    idTeam:number,
    idDay:number,
    activityCount:number,
    // numberOfRoundFromStart?:number,
}