export interface LeaderBoardDetails{
    id?:number,
    result:number,
    idActivity:number,
    idTeam:number,
    idDay:number,
    activityCount:number,
    activityName:string,
    teamName:string,
}