import { Resolve } from "@angular/router";
import { Injectable } from "@angular/core";
import { DayService } from '../services/day.service';
import { Day } from '../models/day';



@Injectable({
    providedIn: 'root'
  })
  
export class DayResolver implements Resolve<Day>{
    constructor(private dayService:DayService){}
    resolve(route: import("@angular/router").ActivatedRouteSnapshot, state: import("@angular/router").RouterStateSnapshot)
    : Day | import("rxjs").Observable<Day> | Promise<Day> {
        return this.dayService.GetById(route.params.id);       
    }  
}