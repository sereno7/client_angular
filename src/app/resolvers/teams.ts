import { Resolve } from "@angular/router";
import { Injectable } from "@angular/core";
import { Team } from '../models/team';
import { TeamService } from '../services/team.service';



@Injectable({
    providedIn: 'root'
  })
  
export class TeamResolver implements Resolve<Team>{
    constructor(private teamService:TeamService){}
    resolve(route: import("@angular/router").ActivatedRouteSnapshot, state: import("@angular/router").RouterStateSnapshot)
    : Team | import("rxjs").Observable<Team> | Promise<Team> {
        let id = route.params.id;  
        return this.teamService.GetById(id);       
    }  
}
