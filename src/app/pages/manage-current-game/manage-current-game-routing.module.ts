import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ManageCurrentGamePage } from './manage-current-game.page';

const routes: Routes = [
  {
    path: '',
    component: ManageCurrentGamePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ManageCurrentGamePageRoutingModule {}
