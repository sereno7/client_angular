import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ManageCurrentGamePageRoutingModule } from './manage-current-game-routing.module';

import { ManageCurrentGamePage } from './manage-current-game.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ManageCurrentGamePageRoutingModule
  ],
  declarations: [ManageCurrentGamePage]
})
export class ManageCurrentGamePageModule {}
