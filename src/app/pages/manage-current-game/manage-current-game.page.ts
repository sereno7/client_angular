import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { HubConnection } from '@aspnet/signalr';
import { SignalRGameService } from 'src/app/services/signal-r-game.service';
import { PlayerMasterPage } from 'src/app/models/AppModel/playerMasterPage';
import { SessionService } from 'src/app/services/session.service';
import { AnimationController, Animation } from '@ionic/angular';
import { LeaderboardService } from 'src/app/services/leaderboard.service';



@Component({
  selector: 'app-manage-current-game',
  templateUrl: './manage-current-game.page.html',
  styleUrls: ['./manage-current-game.page.scss'],
})
export class ManageCurrentGamePage{

  ionViewWillEnter(){
    this.leaderBoardService.GetMaxByActivityCount(this.session.context.value.id, this.session.context.value.countActivity).subscribe(data =>{
      this.getValueToLeaderBoardTab(data);
    })
    this.timerCount = 0;
    this.session.checkSignalR().then(()=>{
      this.onSignalR();
      if(this.roundEnded)
      {
        this.startTimer();
      }
      this.addTeamToTab();
      this.allScoreOk = false; 
      
    }).then(()=>
    {
      setTimeout(() => {
        this.signalrGameService.CheckPauseStatus();
      }, 1000);
    })
  }

  ionViewWillLeave(){
    this.signalrGameService._hubConnection.off("End");
    this.signalrGameService._hubConnection.off("pause");
    this.signalrGameService._hubConnection.off("Discount");
    this.signalrGameService._hubConnection.off("getScoreMaster");
    this.signalrGameService._hubConnection.off("roundIsEnded");
    this.signalrGameService._hubConnection.off("CheckPauseStatus");

  }
  roundEnded:boolean = true;
  anim:Animation;
  pause:boolean;
  timerCount:number;
  count:number;
  allScoreOk:boolean;
  players: PlayerMasterPage[] = [];
  
  
  public _hubConnection: HubConnection; 
  constructor
  (
    private signalrGameService: SignalRGameService,
    private leaderBoardService: LeaderboardService,
    private router:Router,
    protected session:SessionService,
    private animationCtrl: AnimationController,
  ) {}

   
  addTeamToTab(){
    this.players = [];
      for (const i of this.session.context.value.listTeams) {
        this.players.push(({nameT:i.name,idT:i.id, nameA: "", isReady: false, color: i.color}))
    }
  }

startTimer(){
  this.signalrGameService.SendStartTimer();
}

pauseTimer(status:boolean){
  this.signalrGameService.SendTimerPause(status)
}


activityEnd(){
  this.signalrGameService.SendGameOver(this.session.context.value.id);   
}


// ABONNEMENT SIGNALR
onSignalR(){
  //Ecouter le Timer
  this.signalrGameService._hubConnection.on('Discount', discount =>{
    this.timerCount = discount; 
    console.log(this.timerCount);  
  })

  //Ecouter la fin de la partie
  this.signalrGameService._hubConnection.on("End",x =>{
    this.session.refresh(this.session.user.value.idDay);
    this.router.navigateByUrl('/master');
  })

  this.signalrGameService._hubConnection.on("roundIsEnded", data =>{
    this.roundEnded = data;
})

this.signalrGameService._hubConnection.on("CheckPauseStatus", data =>{
    this.pause = data;
    if(data)
    {
      this.animationPlay();
    }
    // else
    // {
    //   this.anim?.stop();
    // }
})

  //Ecouter les scores
  this.signalrGameService._hubConnection.on("getScoreMaster", (data) =>{  
    this.getValueToLeaderBoardTab(data);
    // for (const i of data) {
    //   let p = this.players.find(p => p.idT == i.idTeam)
    //   if(p)
    //   {
    //     p.result = i.result;
    //     p.isReady = true;
    //     p.nameA = i.activityName;
    //   }
    // }
    // this.allScoreOk = this.players.filter(p => p.isReady == false).length == 0;
  })

  //Ecouter le mode pause on/off
  this.signalrGameService._hubConnection.on("Pause", data =>{
    this.pause = data;
    if(data)
    {
      this.animationPlay();
    }
    else
    {
      console.log("stop");
      console.log(this.anim);
      
      this.anim.stop();
    }
  })
}

getValueToLeaderBoardTab(data:any){
  for (const i of data) {
    let p = this.players.find(p => p.idT == i.idTeam)
    if(p)
    {
      p.result = i.result;
      p.isReady = true;
      p.nameA = i.activityName;
    }
  }
  this.allScoreOk = this.players.filter(p => p.isReady == false).length == 0;
}
animationPlay(){
  this.anim = this.animationCtrl.create() 
    .addElement(document.querySelector('#timer'))
    .duration(1000)
    .iterations(Infinity)
    .keyframes([
      { offset: 0, transform: 'scale(1)', opacity: '1' },
      { offset: 0.5, transform: 'scale(1.2)', opacity: '0.3' },
      { offset: 1, transform: 'scale(1)', opacity: '1' }
  ]);
  this.anim.play();
  }

}

