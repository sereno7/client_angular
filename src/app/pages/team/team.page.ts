import { Component } from '@angular/core';
import { SignalRGameService } from 'src/app/services/signal-r-game.service';
import { Router } from '@angular/router';
import { SessionService } from 'src/app/services/session.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivityTeam } from 'src/app/models/activityTeam';
import { Activity } from 'src/app/models/activity';
import { ActivityService } from 'src/app/services/activity.service';
import { DayActivityService } from 'src/app/services/day-activity.service';
import { DayActivitiesModel } from 'src/app/models/dayActivities';
import { TeamService } from 'src/app/services/team.service';
import { Team } from 'src/app/models/team';


@Component({
  selector: 'app-team',
  templateUrl: './team.page.html',
  styleUrls: ['./team.page.scss'],
})
export class TeamPage {
  

  ionViewWillLeave(){
    this.signalrGameService._hubConnection.off("Started");
    // this.signalrGameService._hubConnection.off("ResetRegister");
    this.signalrGameService._hubConnection.off("checkIfReady");
    this.signalrGameService._hubConnection.off("youRegistered");
    this.signalrGameService._hubConnection.off("UnregisterFromActivity");
    this.signalrGameService._hubConnection.off("endDay");
    this.signalrGameService._hubConnection.off("Message");
  }

  ionViewWillEnter(){
    let d:number[] = [];
    this.registered = false;
    this.session.user.subscribe(user =>{
      if(user == null)
        return;
      this.dayActivityService.getByIdDay(user.idDay).subscribe(day =>{
        this.activityList = [];
        this.activityService.getAll().subscribe(all =>{      
          day.forEach(element => {
            d.push(element.idActivity);
        });
        all.forEach(activity => {
          if(d.find(x => x == activity.id))
            this.activityList.push(activity);
        });
        });
      })  
      this.idTeamLocal = user.idTeam;   
      this.teamService.GetById(this.idTeamLocal).subscribe(data =>{
        this.team = data;
      })
      this.registered = false;
      this.ready = false;     
      this.session.checkSignalR().then(()=>{
        this.onSignalR();
        this.WhereIamRegistered();
        this.CheckIfReady();
      })
      
    })
  } 
  registered:boolean = false;
  ready:boolean = false;
  started:boolean;
  activityList:Activity[] = [];
  activityDayList:DayActivitiesModel[];
  // finalList:Activity[];
  // gameModel:GameModel;
  idTeamLocal: number;
  activityForm: FormGroup;
  activityTeam: ActivityTeam;
  team:Team;
  constructor(
    private signalrGameService: SignalRGameService,
    protected session: SessionService,
    private activityService:ActivityService,
    private teamService:TeamService,
    private dayActivityService:DayActivityService,
    private router: Router,) {

      this.activityForm = new FormGroup({
        idActivity : new FormControl(null, [
          Validators.required,
          Validators.maxLength(255),
        ]),
     })  
    }

  getRegisteredActivity(){
    let a:Activity = this.activityList.find(x => x.id == 65)
  }

  register() {
    this.activityTeam = this.activityForm.value; 
    this.signalrGameService.SendNewTeam(this.idTeamLocal, this.activityTeam.idActivity, false);
    this.registered = true;
    this.activityForm.reset();
  }

  beReady(){
    this.signalrGameService.SendReady(this.idTeamLocal, this.session.context.value.countActivity)
    this.ready = true;
  }

  Unregister(){
    this.signalrGameService.Unregister(this.session.user.value.idTeam, this.session.context.value.countActivity);
  }

  WhereIamRegistered(){
    this.signalrGameService.WhereIamRegistered(this.session.user.value.idTeam); 
  }
  CheckIfReady(){
    this.signalrGameService.CheckIfReady(this.session.user.value.idTeam); 
  }
  onSignalR(){
    this.signalrGameService._hubConnection.on("Started", status => {     
      this.session.refresh(this.session.context.value.id);
      if(status)
      {
        this.router.navigateByUrl('/current-game/'+ this.session.user.value.idTeam)
      }    
    });  

    this.signalrGameService._hubConnection.on("Message", msg =>{
      this.session.connexionToast(msg);
    })

    this.signalrGameService._hubConnection.on("endDay", msg =>{
      this.router.navigateByUrl('/end'); 
    })

    this.signalrGameService._hubConnection.on("UnregisterFromActivity", msg =>{
      this.registered = false;       
    })
    // this.signalrGameService._hubConnection.on("ResetRegister", x =>{
    //   this.registered = false;
    // })
    this.signalrGameService._hubConnection.on("youRegistered", x =>{ 
      this.registered = x || 0;
    })
    this.signalrGameService._hubConnection.on("checkIfReady", x =>{
      this.ready = x || 0;
    })
  }  
}
