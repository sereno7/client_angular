import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SlideLeaderboardPageRoutingModule } from './slide-leaderboard-routing.module';

import { SlideLeaderboardPage } from './slide-leaderboard.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SlideLeaderboardPageRoutingModule
  ],
  declarations: [SlideLeaderboardPage]
})
export class SlideLeaderboardPageModule {}
