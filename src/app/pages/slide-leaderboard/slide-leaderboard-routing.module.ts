import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SlideLeaderboardPage } from './slide-leaderboard.page';

const routes: Routes = [
  {
    path: '',
    component: SlideLeaderboardPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SlideLeaderboardPageRoutingModule {}
