import { Component, OnInit, AfterViewInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Activity } from 'src/app/models/activity';
import { LeaderBoardDetailsByActivity } from 'src/app/models/AppModel/LeaderBoardDetailsByActivity';
import { Person } from 'src/app/models/Person';
import { ActivityService } from 'src/app/services/activity.service';
import { DayActivityService } from 'src/app/services/day-activity.service';
import { LeaderboardService } from 'src/app/services/leaderboard.service';
import { SessionService } from 'src/app/services/session.service';

@Component({
  selector: 'app-slide-leaderboard',
  templateUrl: './slide-leaderboard.page.html',
  styleUrls: ['./slide-leaderboard.page.scss'],
})
export class SlideLeaderboardPage implements OnInit, AfterViewInit {
  listActivity:Activity[];
  listLeaderBoardByActivity:LeaderBoardDetailsByActivity[];
  currentTeam:Person;
  activityList:Activity[] = [];
  constructor(
    private leaderboardService: LeaderboardService,
    private sessionService:SessionService,
    private activityService:ActivityService,
    private modal: ModalController,
    private dayActivityService:DayActivityService,
    protected session: SessionService,
    ) { }
    
    ngAfterViewInit(): void {
      this.getAllActivity();
      // this.slideChanged(); 
    }

  ngOnInit() {  
  }

  slideChanged() {
    let id = this.getIdActivityBySlide();
    if(id == null)
      return;
    this.leaderboardGetMaxByActivity(this.sessionService.user.value.idDay,id);
  }


  getAllActivity(){
    let d:number[] = [];
    this.session.user.subscribe(user =>{
      if(user == null)
        return;
    this.dayActivityService.getByIdDay(user.idDay).subscribe(day =>{
      this.activityList = [];
      this.activityService.getAll().subscribe(all =>{      
        day.forEach(element => {
          d.push(element.idActivity);
      });
      all.forEach(activity => {
        if(d.find(x => x == activity.id))
          this.activityList.push(activity);
      });
      });
    })
    })  


    // this.activityService.getAll().subscribe(data =>{
    //   this.listActivity = data; 
    // })
  }


  // getAllActivity(){
  //   this.activityService.getAll().subscribe(data =>{
  //     this.listActivity = data; 
  //   }, error => {
      
  //   })
  // }

  getIdActivityBySlide(){
    //recuperer la valeur de la propriété ID sur l'élement de la classe .swiper-slide-active
    let current = document.querySelector('.swiper-slide-active');
    if(current == null)
      return;
    return parseInt(current.id);
  }

  leaderboardGetMaxByActivity(idD:number, idA:number){
    this.leaderboardService.GetMaxByActivity(idD,idA).subscribe(data =>{
      this.listLeaderBoardByActivity = data;  
    })
  }

  close(){
    this.modal.dismiss();
  } 

  // SETTINGS OF SLIDE
  slideOpts = {
    on: {
      beforeInit() {
        const swiper = this;
        swiper.classNames.push(`${swiper.params.containerModifierClass}flip`);
        swiper.classNames.push(`${swiper.params.containerModifierClass}3d`);
        const overwriteParams = {
          slidesPerView: 1,
          slidesPerColumn: 1,
          slidesPerGroup: 1,
          watchSlidesProgress: true,
          spaceBetween: 0,
          virtualTranslate: true,
        };
        swiper.params = Object.assign(swiper.params, overwriteParams);
        swiper.originalParams = Object.assign(swiper.originalParams, overwriteParams);
      },
      setTranslate() {
        const swiper = this;
        const { $, slides, rtlTranslate: rtl } = swiper;
        for (let i = 0; i < slides.length; i += 1) {
          const $slideEl = slides.eq(i);
          let progress = $slideEl[0].progress;
          if (swiper.params.flipEffect.limitRotation) {
            progress = Math.max(Math.min($slideEl[0].progress, 1), -1);
          }
          const offset$$1 = $slideEl[0].swiperSlideOffset;
          const rotate = -180 * progress;
          let rotateY = rotate;
          let rotateX = 0;
          let tx = -offset$$1;
          let ty = 0;
          if (!swiper.isHorizontal()) {
            ty = tx;
            tx = 0;
            rotateX = -rotateY;
            rotateY = 0;
          } else if (rtl) {
            rotateY = -rotateY;
          }
  
           $slideEl[0].style.zIndex = -Math.abs(Math.round(progress)) + slides.length;
  
           if (swiper.params.flipEffect.slideShadows) {
            // Set shadows
            let shadowBefore = swiper.isHorizontal() ? $slideEl.find('.swiper-slide-shadow-left') : $slideEl.find('.swiper-slide-shadow-top');
            let shadowAfter = swiper.isHorizontal() ? $slideEl.find('.swiper-slide-shadow-right') : $slideEl.find('.swiper-slide-shadow-bottom');
            if (shadowBefore.length === 0) {
              shadowBefore = swiper.$(`<div class="swiper-slide-shadow-${swiper.isHorizontal() ? 'left' : 'top'}"></div>`);
              $slideEl.append(shadowBefore);
            }
            if (shadowAfter.length === 0) {
              shadowAfter = swiper.$(`<div class="swiper-slide-shadow-${swiper.isHorizontal() ? 'right' : 'bottom'}"></div>`);
              $slideEl.append(shadowAfter);
            }
            if (shadowBefore.length) shadowBefore[0].style.opacity = Math.max(-progress, 0);
            if (shadowAfter.length) shadowAfter[0].style.opacity = Math.max(progress, 0);
          }
          $slideEl
            .transform(`translate3d(${tx}px, ${ty}px, 0px) rotateX(${rotateX}deg) rotateY(${rotateY}deg)`);
        }
      },
      setTransition(duration) {
        const swiper = this;
        const { slides, activeIndex, $wrapperEl } = swiper;
        slides
          .transition(duration)
          .find('.swiper-slide-shadow-top, .swiper-slide-shadow-right, .swiper-slide-shadow-bottom, .swiper-slide-shadow-left')
          .transition(duration);
        if (swiper.params.virtualTranslate && duration !== 0) {
          let eventTriggered = false;
          // eslint-disable-next-line
          slides.eq(activeIndex).transitionEnd(function onTransitionEnd() {
            if (eventTriggered) return;
            if (!swiper || swiper.destroyed) return;
  
            eventTriggered = true;
            swiper.animating = false;
            const triggerEvents = ['webkitTransitionEnd', 'transitionend'];
            for (let i = 0; i < triggerEvents.length; i += 1) {
              $wrapperEl.trigger(triggerEvents[i]);
            }
          });
        }
      }
    }
  };
}
