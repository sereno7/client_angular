import { Component, ElementRef, ViewChild } from '@angular/core';
import { SignalRGameService } from 'src/app/services/signal-r-game.service';
import { Day } from 'src/app/models/day';
import { AnimationController, Animation, AlertController } from '@ionic/angular';
import { tabTeamReadyModel } from 'src/app/models/AppModel/tabTeamIsReady';
import { SessionService } from 'src/app/services/session.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-master',
  templateUrl: './master.page.html',
  styleUrls: ['./master.page.scss'],
})
export class MasterPage{

  ionViewDidEnter()
  {
    this.players = [];
    this.session.checkSignalR().then(()=>{
      this.createTab();
      this.allIsReady = false;
      this.onSignalR();
      this.signalrGameService.updateListTeamReady(this.session.context.value.countActivity);
    });
  }

  ionViewWillLeave()
  {
    this.signalrGameService._hubConnection.off("NewTeam");
    this.signalrGameService._hubConnection.off("Started");
    this.signalrGameService._hubConnection.off("Unregister");
  }

  players: tabTeamReadyModel[] = [];
  allIsReady :boolean = false;
  gameNumber:number;
  idDay:number;
  currentDay:Day;
  anim:Animation;

  //crée une référence
  @ViewChild("play")
  playIcon:ElementRef;
  
  constructor
  (
    private signalrGameService: SignalRGameService,
    private router: Router,
    private animationCtrl: AnimationController,
    protected session: SessionService,
    public alertController: AlertController
  ){}
  
  gameStarted(){
    this.signalrGameService.SendStartGame(this.session.context.value.id);
  }

  endDay(){
    this.presentAlertConfirm();
  }

  onSignalR(){
    this.signalrGameService._hubConnection.on("NewTeam", (data) => { 
      this.createTab();          
      for (const i of data) {
        let p = this.players.find(p => p.id == i.id); 
        if(p)
        {
          p.nameCurrentActivity = i.nameCurrentActivity;
          p.color = i.color;
          p.isReady = i.isReady;
        }
        this.allIsReady = this.players.filter(p => p.isReady == false).length == 0;
        console.log(this.allIsReady);
        
        if(this.allIsReady)
          this.animationPlay();
      } 
    });  
    
    // Listen Game started => Go to MANAGE And STOP anim
    this.signalrGameService._hubConnection.on("Started", data =>{
      this.anim?.stop();
      this.router.navigateByUrl('/manage-current-game');    
    })
    //Listen Unregistered
    this.signalrGameService._hubConnection.on("Unregister", data =>{   
      this.signalrGameService.updateListTeamReady(this.session.context.value.countActivity);
    })
  }
  
  private async createTab(){
    this.players = [];
    for (const i of this.session.context.value.listTeams) {
      let teamModel: tabTeamReadyModel = (
        {
          name:i.name,
          id:i.id, 
          nameCurrentActivity: "", 
          isReady: false, 
          color: i.color,
        })           
      this.players.push(teamModel)
    };
  }

  animationPlay(){
    this.anim = this.animationCtrl.create() 
      .addElement(this.playIcon.nativeElement)
      .duration(1000)
      .iterations(Infinity)
      .keyframes([
        { offset: 0, transform: 'scale(1)', opacity: '1' },
        { offset: 0.5, transform: 'scale(1.2)', opacity: '0.3' },
        { offset: 1, transform: 'scale(1)', opacity: '1' }
    ]);
    this.anim.play();
  }

  async presentAlertConfirm() {    
    let alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Confirmation!',
      message: 'Etes vous sûr de vouloir terminer la journée? Cette action est irréversible.',
      buttons: [
        {
          text: 'Annuler',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Annuler');
          }
        }, {
          text: 'End',
          handler: () => {
            this.signalrGameService.SendEndDay(this.session.context.value.id);
            this.router.navigateByUrl('/end'); 
          }
        }
      ]
    });
    await alert.present();
}
}
