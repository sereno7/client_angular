import { Component, HostListener, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Activity } from 'src/app/models/activity';
import { LeaderBoard } from 'src/app/models/leaderBoard';
import { Team } from 'src/app/models/team';
import { ActivityService } from 'src/app/services/activity.service';
import { LeaderboardService } from 'src/app/services/leaderboard.service';
import { SignalRGameService } from 'src/app/services/signal-r-game.service';
import { SessionService } from 'src/app/services/session.service';
import { LeaderBoardDetailsByActivity } from 'src/app/models/AppModel/LeaderBoardDetailsByActivity';
import { AnimationController, Animation } from '@ionic/angular';

@Component({
  selector: 'app-current-game',
  templateUrl: './current-game.page.html',
  styleUrls: ['./current-game.page.scss'],
})
export class CurrentGamePage{


  ionViewWillEnter() {
    this.team = this.route.snapshot.data.resolveTeam;
    this.session.checkSignalR().then(() => {
      this.onSignalR();
      this.timerCount = 0;
      this.score = 0;
      this.scoreSended = false;
      this.session.refresh(this.session.user.value.idDay);
      this.activityGetById();
      this.leaderboardGetMaxByActivity();
      this.session.context.subscribe(data => {
        // this.signalrGameService.CheckPauseStatus();
        this.signalrGameService.RoundIsEnded();
        this.signalrGameService.ScoreAlreadySended(this.team.idDay, this.team.id, data.countActivity);
        setTimeout(() => {
          this.signalrGameService.CheckPauseStatus();
        }, 1000);
      });
    });
  }

  ionViewWillLeave() {
    this.signalrGameService._hubConnection.off("End");
    this.signalrGameService._hubConnection.off("getScore");
    this.signalrGameService._hubConnection.off("Pause");
    this.signalrGameService._hubConnection.off("Discount");
    this.signalrGameService._hubConnection.off("roundIsEnded");
    this.signalrGameService._hubConnection.off("scoreAlreadySended");
    this.signalrGameService._hubConnection.off("CheckPauseStatus");
  }
  anim: Animation;
  pause: boolean;
  timerCount: number;
  team: Team;
  currentActivity: Activity;
  roundEnded: boolean = false;
  score: number;
  form: FormGroup;
  leaderboard: LeaderBoardDetailsByActivity[] = null;
  scoreSended: boolean;

  constructor(
    private signalrGameService: SignalRGameService,
    private activityService: ActivityService,
    private route: ActivatedRoute,
    private router: Router,
    protected session: SessionService,
    private leaderboardService: LeaderboardService,
    private animationCtrl: AnimationController
  ) {
    this.form = new FormGroup({
      score: new FormControl(null, [
        Validators.required,
        Validators.maxLength(255),
      ]),
    });
  }




  sendScore() {
    if (this.form.valid) {
      let g: LeaderBoard = {
        result: this.form.value.score,
        idActivity: this.team.currentActivity,
        idTeam: this.team.id,
        idDay: this.team.idDay,
        activityCount: this.session.context.value.countActivity,
      };
      this.signalrGameService.sendScoreSignalR(g);
      this.scoreSended = true;
      this.form.reset();
    }
  }

  onSignalR() {
    this.signalrGameService._hubConnection.on("End", end => {
      this.session.refresh(this.session.user.value.idDay);
      this.router.navigateByUrl('/team');
    });

    this.signalrGameService._hubConnection.on("getScore", data => {
      this.leaderboard = data;
    });
    this.signalrGameService._hubConnection.on('Discount', discount => {
      this.timerCount = discount;
      if (this.timerCount <= 0)
        this.roundEnded = true;
    });

    //Ecouter le mode pause on/off
    this.signalrGameService._hubConnection.on("Pause", data => {
      this.pause = data;
      console.log(data);
      
      if (data)
        this.animationPlay();

      else {
        console.log(this.anim);
        console.log("stop");
        this.anim.stop();
        this.anim.destroy();
      }
    });

    this.signalrGameService._hubConnection.on("roundIsEnded", data => {
      this.roundEnded = data;
    });
    this.signalrGameService._hubConnection.on("scoreAlreadySended", data => {
      this.scoreSended = data;
    });
    this.signalrGameService._hubConnection.on("CheckPauseStatus", data => {
      this.pause = data;
      if (data)
        this.animationPlay();
      // else
      //   this.anim?.destroy()
    });
  }

  leaderboardGetMaxByActivity() {
    this.leaderboardService.GetMaxByActivity(this.team.idDay, this.team.currentActivity).subscribe(data => {
      this.leaderboard = data;
    });
  }

  activityGetById() {
    this.activityService.getById(this.team.currentActivity).subscribe(data => {
      this.currentActivity = data;
    });
  }

  animationPlay() {
    this.anim = this.animationCtrl.create()
      .addElement(document.querySelector('#timer'))
      .duration(1000)
      .iterations(Infinity)
      .keyframes([
        { offset: 0, transform: 'scale(1)', opacity: '1' },
        { offset: 0.5, transform: 'scale(1.2)', opacity: '0.3' },
        { offset: 1, transform: 'scale(1)', opacity: '1' }
      ]);
    this.anim.play();
  }
}
