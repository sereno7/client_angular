import { Component, OnInit, AfterViewInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ToastController } from '@ionic/angular';
import { AuthService } from 'src/app/services/auth.service';
import { SessionService } from 'src/app/services/session.service';


@Component({
  selector: 'app-logon',
  templateUrl: './logon.page.html',
  styleUrls: ['./logon.page.scss'],
})
export class LogonPage implements OnInit, AfterViewInit {
  loginForm: FormGroup;
  constructor(
    private authService: AuthService,
    private toastController: ToastController,
    protected session : SessionService,
    ) 
    {
      this.createForm();
    }
    
    ngAfterViewInit(): void {
      
    }

    ngOnInit() { 

  }

  submit() {

    if(this.loginForm.valid) {
      this.authService.login(this.loginForm.value).subscribe(token => {
          this.session.start(token);
          this.loginForm.reset()
      }, error => {
        // this.connexionToast(error);     
      })
    }
  }

  async connexionToast(message:string) {
    const toast = await this.toastController.create({
      message: message,
      duration: 2000
    });
    toast.present();
  }

  createForm(){
    this.loginForm = new FormGroup({
      login: new FormControl(null, [
        Validators.required,
        Validators.maxLength(255),
      ]),
      plainPassword: new FormControl(null, [
        Validators.required,
        Validators.maxLength(255),
      ]),
    });
  }

}


