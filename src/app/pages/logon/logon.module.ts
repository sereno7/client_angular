import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { LogonPageRoutingModule } from './logon-routing.module';
import { LogonPage } from './logon.page';
import { IonicStorageModule } from '@ionic/storage';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LogonPageRoutingModule,
    ReactiveFormsModule,
    IonicStorageModule,
  ],
  declarations: [LogonPage]
})
export class LogonPageModule {}
