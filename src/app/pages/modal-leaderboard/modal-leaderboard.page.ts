import { Component, OnInit, ViewChild } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { LeaderBoardTotalByTeam } from 'src/app/models/AppModel/leaderBoardTotalByTeam';
import { LeaderboardService } from 'src/app/services/leaderboard.service';
import { SessionService } from 'src/app/services/session.service';
import { SignalRGameService } from 'src/app/services/signal-r-game.service';
import { Chart } from 'chart.js';

@Component({
  selector: 'app-modal-leaderboard',
  templateUrl: './modal-leaderboard.page.html',
  styleUrls: ['./modal-leaderboard.page.scss'],
})
export class ModalLeaderboardPage implements OnInit {
  @ViewChild('barChart') barChart;
  label:string[];
  result:number[];
  color:string[];
  bars: any;
  colorArray: any;
  tabTotal:LeaderBoardTotalByTeam[];
  idDay:number;
  constructor(
    private modal: ModalController,
    private leaderBoardService: LeaderboardService,
    private session: SessionService,
    private signalRService: SignalRGameService,
    ) { }

  ionViewWillEnter(){
    this.label = [];
    this.result = [];
    this.color = [];
    this.leaderboardGetLeaderBoardTotalByTeam(this.idDay);
  }
  ionViewDidEnter() {
  this.createBarChart(); 
  }
  ngOnInit() {
    this.tabTotal = [];
    this.session.user.subscribe(user =>{
      if(user == null)
        return;
      this.idDay = user.idDay;            
      this.onSignalR()     
    })
  }
  close(){
    this.modal.dismiss();
  } 

  onSignalR(){
    this.signalRService._hubConnection.on('getScore', () => {
      this.leaderBoardService.GetLeaderBoardTotalByTeam(this.idDay).subscribe(data =>{
        this.tabTotal = data;    
        data.forEach(element => {
          this.label.push(element.teamName);
          this.result.push(element.total); 
          this.color?.push(element.color);
        });   
      })  
    })  
  }

  leaderboardGetLeaderBoardTotalByTeam(idD:number){
    this.leaderBoardService.GetLeaderBoardTotalByTeam(idD).subscribe(data =>{     
      this.tabTotal = data;  
      data.forEach(element => {
        this.label?.push(element.teamName);
        this.result?.push(element.total);
        this.color?.push(element.color);   
      });       
    })  
  }
  createBarChart() {
    this.bars = new Chart(this.barChart.nativeElement, {
      type: 'horizontalBar',
      data: {
        labels: this.label,
        datasets: [{
          label: '',
          data: this.result,
          // backgroundColor: '#75729D', // array should have same number of elements as number of dataset
          backgroundColor: this.color, // array should have same number of elements as number of dataset
          borderColor: '#75729D',// array should have same number of elements as number of dataset
          borderWidth: 1
        }]
      },
      options: {
        legend: {
          display: false
      },
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero: true
            }
          }]
        }
      }
    });
  }
}
