import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ModalLeaderboardPageRoutingModule } from './modal-leaderboard-routing.module';

import { ModalLeaderboardPage } from './modal-leaderboard.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ModalLeaderboardPageRoutingModule
  ],
  declarations: [ModalLeaderboardPage]
})
export class ModalLeaderboardPageModule {}
