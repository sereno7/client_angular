import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ModalLeaderboardPage } from './modal-leaderboard.page';

const routes: Routes = [
  {
    path: '',
    component: ModalLeaderboardPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ModalLeaderboardPageRoutingModule {}
