import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { SessionService } from '../services/session.service';
import { ToastController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class IsLoggedGuard implements CanActivate {

  constructor(
    private session: SessionService,
    private toastController: ToastController,
    private route:Router,
    ){}
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {  
      if(this.session.user.value != null) {        
          return true;
      }
      else{
        this.route.navigateByUrl('logon')
        this.connexionToast("Login Required").then(() =>{
          return false;
        })
      }   
  }

  async connexionToast(message:string) {
    const toast = await this.toastController.create({
      message: message,
      duration: 1000
    });
    toast.present();
  }
  
}
