import { Component, OnInit } from '@angular/core';

import { Platform, ToastController, MenuController, ModalController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { Storage } from '@ionic/storage';
import { SessionService } from './services/session.service';
import { Router } from '@angular/router';
import { ModalLeaderboardPage } from './pages/modal-leaderboard/modal-leaderboard.page';
import { SlideLeaderboardPage } from './pages/slide-leaderboard/slide-leaderboard.page';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent implements OnInit {
  public selectedIndex = 0;
  // public conn: BehaviorSubject<Boolean>;
  // public connected:boolean;
  public appPages = [
    {
      title: 'Home',
      url: '/home',
      icon: 'home'
    },
    // {
    //   title: 'Start',
    //   url: '/start',
    //   icon: 'edit',
    // },
    // {
    //   title: 'Acceuil',
    //   url: '/welcome',
    //   icon: 'heart'
    // },
  ];
  // public labels = ['Family', 'Friends', 'Notes', 'Work', 'Travel', 'Reminders'];

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private storage: Storage,
    public session: SessionService,
    private router: Router,
    private toastController: ToastController,
    private modalController: ModalController
  ) {
    this.initializeApp();
    // this.conn = new BehaviorSubject<Boolean>(null);
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  ngOnInit() {
    const path = window.location.pathname.split('folder/')[1];
    if (path !== undefined) {
      this.selectedIndex = this.appPages.findIndex(page => page.title.toLowerCase() === path.toLowerCase());
    }
  }


  log(){
    if(this.session.user.value)
      this.logout();
    if(!this.session.user.value)
      this.login();
    else
      return;
  }

  async logout(){
    await this.session.stop();
    this.router.navigate(['/logon']);
    this.connexionToast("Vous êtes à présent déconnecté");
  }

  login(){
    this.router.navigateByUrl('/logon');
    this.connexionToast("Veuillez vous connecter");
  }

  async connexionToast(message:string) {
    const toast = await this.toastController.create({
      message: message,
      duration: 2000
    });
    toast.present();
  }

  async presentModal() {
    if(this.session.user.value)
    {
      const modal = await this.modalController.create({
        component: ModalLeaderboardPage,
        cssClass: 'my-custom-class'
      });
      return await modal.present();
    }
    else
      this.connexionToast("Vous devez être connecté")
  }

  async presentModalSlide() {
    if(this.session.user.value)
    {
      const modal = await this.modalController.create({
        component: SlideLeaderboardPage,
        cssClass: 'my-custom-class'
      });
      return await modal.present();
    }
    else
      this.connexionToast("Vous devez être connecté")  
  }
}


