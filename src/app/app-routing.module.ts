import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { DayResolver } from './resolvers/dayResolver';
import { TeamResolver } from './resolvers/teams';
import { IsLoggedGuard } from './guards/is-logged.guard';


const routes: Routes = [
  {
    path: '',
    redirectTo: '/logon',
    pathMatch: 'full'
  },
  {
    path: 'master',
    // resolve:{ 
    //   resolveDay :DayResolver,
    // },
    loadChildren: () => import('./pages/master/master.module').then( m => m.MasterPageModule),
    canActivate: [ IsLoggedGuard ]
  },
  {
    path: 'team',
    loadChildren: () => import('./pages/team/team.module').then( m => m.TeamPageModule),
    canActivate: [ IsLoggedGuard ]
  },
  {
    path: 'current-game/:id',
    resolve:{ 
      resolveTeam :TeamResolver,
    },
    loadChildren: () => import('./pages/current-game/current-game.module').then( m => m.CurrentGamePageModule),
    canActivate: [ IsLoggedGuard ]
  },
  {
    path: 'manage-current-game',
    loadChildren: () => import('./pages/manage-current-game/manage-current-game.module').then( m => m.ManageCurrentGamePageModule),
    canActivate: [ IsLoggedGuard ]
  },
  {
    path: 'logon',
    loadChildren: () => import('./pages/logon/logon.module').then( m => m.LogonPageModule)
  },
  {
    path: 'modal-leaderboard',
    loadChildren: () => import('./pages/modal-leaderboard/modal-leaderboard.module').then( m => m.ModalLeaderboardPageModule),
  },
  {
    path: 'slide-leaderboard',
    loadChildren: () => import('./pages/slide-leaderboard/slide-leaderboard.module').then( m => m.SlideLeaderboardPageModule),
    
  },
  {
    path: 'end',
    loadChildren: () => import('./pages/end/end.module').then( m => m.EndPageModule),
    canActivate: [ IsLoggedGuard ]
  },

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
