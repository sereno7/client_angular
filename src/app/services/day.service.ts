import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Day } from '../models/day';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DayService {

  // context: BehaviorSubject<Day[]>
  private url: string = environment.URL_API_DAY;

  constructor(private http: HttpClient) { 
    // this.context = new BehaviorSubject<Day[]>(null);
    // this.refresh();
  }

  // refresh(){
  //   this.http.get<Day[]>(this.url).subscribe(data => {
  //     this.context.next(data);
  //   });
  // }

  GetById(id: number){
    return this.http.get<Day>(this.url + id);  
  }
  

  // incrementCountActivity(id:number){
  //   return this.http.put<Day>(this.url, id)
  // }

  // updateGameStatus(idD:number, idS:number){
  //   let test = {idD: idD, idS: idS}
  //   this.http.put<Day>(this.url + "UpdateStatus/", test).subscribe();   
  // }


  
}
