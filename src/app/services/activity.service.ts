import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Activity } from '../models/activity';
import { catchError }from 'rxjs/operators'

@Injectable({
  providedIn: 'root'
})
export class ActivityService {
  // context: BehaviorSubject<Activity[]>
  private url: string = environment.URL_API_ACTIVITY;

  constructor(private http: HttpClient) {
    // this.context = new BehaviorSubject<Activity[]>(null);
    // this.refresh();
   }

  //  refresh(){
  //   this.http.get<Activity[]>(this.url).subscribe(data => {
  //     this.context.next(data);
  //   });
  // }
  
  getAll(){
    return this.http.get<Activity[]>(this.url);
  }

  // getAll(){
  //   return this.http.get<Activity[]>(this.url).pipe(catchError(error =>{
  //       // Envoyer un toast de error
  //       return error
  //   }))
  // }


  getById(id:number){
    return this.http.get<Activity>(this.url + id);
  }
}
