import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Team } from '../models/team';

@Injectable({
  providedIn: 'root'
})
export class TeamService {
  private url: string = environment.URL_API_TEAM
  constructor(private http: HttpClient) { }

  // GetTeam(){
  //   return this.http.get<Team[]>(this.url);  
  // }

  GetById(id: number){
    return this.http.get<Team>(this.url + id); 
  }
  
  // update(t:number, a:number){
  //   let team:Team={ id:t, currentActivity:a};  
  //   this.http.put<Team>(this.url, team).subscribe();
  // }
}

