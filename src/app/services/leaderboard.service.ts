import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { LeaderBoardDetailsByActivity } from '../models/AppModel/LeaderBoardDetailsByActivity';
import { LeaderBoardTotalByTeam } from '../models/AppModel/leaderBoardTotalByTeam';
import { LeaderBoard } from '../models/leaderBoard';
import { LeaderBoardDetails } from '../models/leaderBoardDetails';

@Injectable({
  providedIn: 'root'
})
export class LeaderboardService {
  private url: string = environment.URL_API_LEADERBOARD;
  constructor(private http:HttpClient) { }

  // getAllResult(){
  //   this.http.get<LeaderBoard[]>(this.url);
  // }

  // addResult(lb:LeaderBoard){    
  //   return this.http.post<LeaderBoard>(this.url, lb)
  // }

  getLeaderBoardDetailsByActivityId(id:number)
  {
    return this.http.get<LeaderBoardDetails[]>(this.url + id)
  }
  
  GetLeaderBoardTotalByTeam(id:number): Observable<LeaderBoardTotalByTeam[]>
  {
    return this.http.get<LeaderBoardTotalByTeam[]>(this.url+ "GetTotalById/" + id)
  }

  GetMaxByActivity(idD:number, idA:number)
  {
    return this.http.get<LeaderBoardDetailsByActivity[]>(this.url+ "GetMaxByActivity/" + idD + "/" + idA)
  }

  GetMaxByActivityCount(idD:number, idC:number)
  {
    return this.http.get<LeaderBoardDetailsByActivity[]>(this.url+ "GetMaxByActivityCount/" + idD + "/" + idC)
  }
  
}
