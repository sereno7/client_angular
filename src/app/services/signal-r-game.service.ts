import { Injectable, EventEmitter } from '@angular/core';
import * as signalR from '@aspnet/signalr';
import { HttpTransportType, HubConnection, HubConnectionBuilder} from '@aspnet/signalr';
import { Storage } from '@ionic/storage';
import { environment } from 'src/environments/environment';
import { LeaderBoard } from '../models/leaderBoard';



@Injectable({
  providedIn: 'root'
})
export class SignalRGameService { 
  
  connectionEstablished = new EventEmitter<Boolean>();  
  public connectionIsEstablished = false;  
  public _hubConnection: HubConnection;  
  url: string = environment.URL_SIGNALR_GAME;
  constructor(private storage : Storage) 
  { 
    this.storage.get('TOKEN').then((x:string) => {
      if(x != null) 
      {
        this.createConnection(x).then(()=>{
          this.startConnection()})         
      }    
  });
  }
  
  private async createConnection(token:string) 
  {  
    const options: signalR.IHttpConnectionOptions = {
      accessTokenFactory: () => {
        return token;
      }
    };
    this._hubConnection = new HubConnectionBuilder() 
      .withUrl(this.url, options)                 
      .build();     
    }
  
  public async startConnection(): Promise<any> 
  { 
    return  new Promise((resolve, reject) => {
      this._hubConnection     
        .start()  
        .then(() => {  
          this.connectionIsEstablished = true;  
          console.log('Hub connection started');  
          // this.connectionEstablished.emit(true);
          resolve(true);  
          // console.log('connection emit');  
        })  
        .catch(err => {  
          console.log('Error while establishing connection, retrying...');
          setTimeout(() => {
            this.startConnection();       
          }, 1000)
        });
    })
      
  }  
  

  SendNewTeam(id: number, idA: number, ready:boolean) 
  { 
    this._hubConnection.invoke('SendNewTeam', id, idA, ready);
  }

  SendReady(idT: number, idC:number)
  {
    this._hubConnection.invoke('SendReady', idT, idC)
  }

  Unregister(idT: number, idC: number)
  {
    console.log(44);
    
    this._hubConnection.invoke('Unregister', idT, idC);
  }

  SendStartGame(id:number)
  {
    this._hubConnection.invoke('SendStartGame', id);
  }
  SendEndDay(id:number)
  {
    this._hubConnection.invoke('SendEndDay', id);
  }

  SendDiscounter(nb : number)
  {
    this._hubConnection.invoke('SendDiscounter', nb);
  }

  SendStartTimer()
  {
    this._hubConnection.invoke('SendStartTimer');
  }

  SendTimerPause(status:boolean)
  {
    this._hubConnection.invoke('SendPauseAndResume', status)
  }

  SendGameOver(id:number)
  {
    this._hubConnection.invoke('SendGameOver', id);
  }

  sendScoreSignalR(g : LeaderBoard)
  {
    this._hubConnection.invoke("SendScoreSignalR", g)
  }

  updateListTeamReady(id:number)
  {
    this._hubConnection.invoke("updateListTeamReady", id) 
  }
  
  public stopConnection()
  {
    this._hubConnection.stop();
  }

  WhereIamRegistered(id:number){
    this._hubConnection.invoke("WhereIamRegistered", id)
  }
  CheckIfReady(id:number){
    this._hubConnection.invoke("CheckIfReady", id)
  }

  RoundIsEnded(){
    this._hubConnection.invoke("RoundIsEnded")
  }

  CheckPauseStatus(){
    this._hubConnection.invoke("CheckPauseStatus")
  }

  ScoreAlreadySended(idD:number,idT:number,aCount:number){
    this._hubConnection.invoke("ScoreAlreadySended", idD, idT, aCount)
  }
}  

