import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { DayActivitiesModel } from '../models/dayActivities';

@Injectable({
  providedIn: 'root'
})
export class DayActivityService {
  private url:string = environment.DAYACTIVITY
  constructor(private http:HttpClient) { }

  getByIdDay(id:number)
  {
    return this.http.get<DayActivitiesModel[]>(this.url+id);
  }
}
