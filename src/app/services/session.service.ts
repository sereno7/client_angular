import { Injectable } from '@angular/core';
import { Person } from '../models/Person';
import jwt_decode from "jwt-decode";
import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { AlertController, ToastController } from '@ionic/angular';
import { DayService } from './day.service';
import { AuthService } from './auth.service';
import { Day } from '../models/day';
import { SignalRGameService } from './signal-r-game.service';
import { TeamService } from './team.service';



@Injectable({
  providedIn: 'root'
})
export class SessionService {
  
  private  _context: BehaviorSubject<Day>
  public get context() : BehaviorSubject<Day>{
    return this._context
  }
  
  private _user: BehaviorSubject<Person>;
  public get user() : BehaviorSubject<Person> {
    return this._user;
  }
  color:string;
  public token : string;
  status:number;

  constructor
  (
    private storage: Storage,
    private route: Router,
    private alertController: AlertController,
    private dayService:DayService,
    private teamService:TeamService,
    private toastController:ToastController,
    private authService:AuthService,
    private signalrGameService:SignalRGameService,  
  ) 
  {
    this._context = new BehaviorSubject<Day>(null);
    this._user = new BehaviorSubject<Person>(null);
    
    // Check if 'TOKEN' exist in STORAGE 
    // If exist +> store USER in STORE and REFRESH CONTEXT(DAY) + ALERT TO REDIRECT
    this.storage.get('TOKEN').then((x) => {
      this.token = x;
      if(this.token != null) 
      {
        this._user.next(this.decode(this.token))
        this.refresh(this.user.value.idDay);
        this.presentAlertConfirm(this._user.value.name);
        this.getColor(this._user?.value?.idTeam);
      }
      
  })
}
  
  // Refresh CONTEXT (Observable of DAY)
    async refresh(id:number) {
      return new Promise((resolve, reject) => {
        this.dayService.GetById(id).subscribe((data) =>{
          this.context.next(data);
          resolve(true);
        }, error => {
          reject()
        })
      })
    };
    
    
    // Decode TOKEN and return a PERSON where properties = claims
    private decode(token) : Person {
      let decoded = jwt_decode(token);
      return {
        id : decoded["id"],
        name: decoded["http://schemas.xmlsoap.org/ws/2005/05/identity/claims/name"],
        idTeam: decoded["idTeams"],
        idDay:decoded["idDay"],
        role: decoded["http://schemas.microsoft.com/ws/2008/06/identity/claims/role"]
      }
    }
    
    // Put TOKEN in STORAGE and refresh USER & CONTEXT in STORE
    public async start(token :string) {
      console.log(token);
      await this.storage.set('TOKEN',token);
      this._user.next(this.decode(token));      
      this.refresh(this._user.value.idDay).then(() => {
        this.getColor(this._user?.value?.idTeam);
        this.connexionToast("Connection OK");
        this.checkStatusUserAndDayAndRedirect();       
      }).catch(error => {
        this.connexionToast("Connection Not OK");
      });
  }

  // Logoff SESSION in API + Clear USER & DAY in STORE + Remove TOKEN in STORAGE
  stop() {
    this.authService.logOff(this._user.value.id);
    this._user.next(null);
    this._context.next(null);
    return this.storage.remove('TOKEN');
  }

  // Alert if a USER is already in STORE
  async presentAlertConfirm(compte:string) {  
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Confirmation :',
      message: 'Vous êtes déjà connecté sous le compte '+ compte,
      buttons: [
        {
          text: 'Sortir',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            this.logout();
          }
        }, {
          text: 'Rejoindre l\'évenement',
          handler: () => {
            this.checkStatusUserAndDayAndRedirect()
        }
      }
    ]
  });
    await alert.present();
  }


  async logout(){
    await this.stop();
    this.route.navigateByUrl('/logon');
    this.connexionToast("Vous êtes à présent déconnecté");
  }

  async connexionToast(message:string) {
    const toast = await this.toastController.create({
      message: message,
      duration: 1000
    });
    toast.present();
  }

  //Check ROLE of USER in STORAGE + Redirect by STATUS of GAME
  checkStatusUserAndDayAndRedirect(){   
    if(this.user.value.role == "ANIMATOR")
    {
      console.log(this.context.value?.status);
      
      if(this.context.value?.status == 1 || this.context.value?.status == 0)
        this.route.navigateByUrl('/team')
      if(this.context.value?.status == 2)
        this.route.navigateByUrl('/current-game/'+ this.user.value.idTeam) 
      if(this.context.value?.status == 3)
        this.route.navigateByUrl('/end')
    }
    else if(this.user.value.role == "MASTER")
    {
      if(this.context.value?.status == 1 || this.context.value?.status == 0)
        this.route.navigateByUrl('/master')
      if(this.context.value?.status == 2)
        this.route.navigateByUrl('/manage-current-game')
      if(this.context.value?.status == 3)
        this.route.navigateByUrl('/end')
    }
  }

  // Check SignalR Status+Reconnect
  public async checkSignalR() {  
    if(this.signalrGameService._hubConnection.state != 1)
    {
      await this.signalrGameService.startConnection();
    }  
  }

  getColor(id:number){
    if(this._user.value.role == 'ANIMATOR')
    {
      this.teamService.GetById(id).subscribe(data =>{
        this.color = data.color;
      })
    }
  }
}
