import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Login } from '../models/login';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private url: string = environment.URL_API_SECURITY;
  constructor(
    private http: HttpClient,
  ) { }

  public login(model: Login): Observable<string> {
    return this.http.post<string>(this.url + 'login', model)
    }


  public logOff(id:number){
    return this.http.get(this.url + 'logoff/'+id ).subscribe();
}

}

